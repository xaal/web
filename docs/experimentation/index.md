# Experimenations

_Testing xAAL_

---

## Show cases
### PRECIOUS Living Lab Demo @Experiment’HAAL
The living lab [Experiment’Haal](https://www.imt-atlantique.fr/recherche/plates-formes_technologiques/experiment-haal/) (Human Ambient Assisting Living) is used in [PRECIOUS](http://www.thepreciousproject.eu/) to realize end-users tests.
A living lab is a laboratory where innovations are proposed to users in a controlled environment.
Experiment’Haal offers an ecosystem to the PRECIOUS project in order to test individual component such as environmental sensors, body weight scale, user feedback, etc.

Below a short video to show the results of our first deployment. Enjoy!

[![Demo Precious project in the Experiment’HAAL living lab](precious.png)](https://vimeo.com/153099732 "Demo Precious project in the Experiment’HAAL living lab")



## The network point of view

### Bandwidth consumed
xAAL is deployed in our [Experiment'Haal](https://www.imt-atlantique.fr/recherche/plates-formes_technologiques/experiment-haal/) living'lab
(about 60 xAAL devices).
According to [results](netprobe.out) ouf our special
[netprobe](https://gitlab.imt-atlantique.fr/xaal/code/c/-/tree/main/netprobe)

device, the mean bandwidth consumed by xAAL is of 71 bytes per second (one message each 3 seconds).


### Bytes count
The idea is to compare xAAL 0.5 to UPnP in terms of packets consumed to perform an action: discover a lamp, then switch it on/off
(hoping this scenario is representative).

Used components for tests:

- xAAL: [dummyDimmer lampCommander](https://gitlab.imt-atlantique.fr/xaal/code/c/-/tree/main/libxaal)
- UPnP: [gupnp-network-light gupnp-universal-cp](https://wiki.gnome.org/Projects/GUPnP)
- wireshark to dump [xAAL messages](xaal_lamp.pcapng) and [UPnP messages](upnp_lamp.pcapng)

Activities:

- Announce of the lamp: UPnP frames 0-51 (15502 bytes), xAAL frames 0-2 (302 bytes)
- Discovering of the lamp: UPnP frames 52-142 (31442 bytes), xAAL frames 3-7 (1353 bytes)
- Switch on: UPnP frames 143-177 (35397 bytes), xAAL frames 8-9 (1868 bytes)
- Switch off: UPnP frames 178-219 (40210 bytes), xAAL frames 10-11 (2385 bytes)

Conclusion: xAAL 0.5 is 94% more "efficient" than a TCP alternative ;-)

Well, to be hosnet in the scenario the UPnP contol point asks the device for its root descriptions (1571 bytes), the description of the power control interface (1080 bytes), the description of the dimmer interface (1353 bytes), and an icon file (1006 bytes).
The xAAL control point doesn't do that, however it may also ask (to a third device, the schema-repository) the the description of the lamp (2568 bytes).

However, if you plan to deploy home-automation solution on lightweight devices, xAAL is fine!
(What's about your STM32? How many simultaneous TCP connections may it support towards UPnP companion devices? ;-) )


### Packets loss
*- Hey, you are saying more efficient, but this is UDP! Without warranty against packets loss! I'm afraid!*

*- Don't panic. Is it really that serious about losing time to time messages? even your messages? And besides, how many are lost?*

Actually, there is no packets loss on Ethernet.
Issues may arise while using [WiFi](https://hal.archives-ouvertes.fr/file/index/docid/84457/filename/RR-5947.pdf) or [HomePlug AV](http://repositorio.upct.es/bitstream/handle/10317/4179/pjpe.pdf).
Their implementation of multicast transmission is criticized for years.

[Dedicated xAAL devices](https://svn.imt-atlantique.fr/repository/xAAL/code/Python/trunk/devices/xaal-netperf) have been coded for tests. The objective is to test real xAAL messages.
Indeed, tools like iperf cannot send packets at a rate so reduced.

Results:

- At home, between a Cubietruck and a Bouygues'Box, with more than 40.000 messages:
- during a full day: `c939c302-e8f8-11e5-89b5-b827ebe8deb7 => 2016-03-13 21:06:38 | msg:40270 msg/sec:1 error:0 neg error:0`
- during a full night: `a5fe9726-e891-11e5-9f1e-b827ebe8deb7 => 2016-03-13 09:44:06 | msg:43593 msg/sec:1 error:0 neg error:0`

The packets loss depends if the WiFi environment is rather quiet (e.g. at home), or crowded (e.g. at work, where every place is covered by at least 3 AP, covering all available frequencies ;-) )
Our experiments conform to [figures annouced by Cisco](http://www.cisco.com/c/en/us/products/collateral/wireless/5508-wireless-controller/white_paper_c11-577721.html), and are even much better, probably due to the extremely low packet rate of xAAL.
Notes that figures are similar if the link is flooded by something else (e.g. ping flood).

Conclusion: packets losses are rare! However, this may append. So, what to do?

- Build resilient applications and devices. According to distributed architecture principles, a component cannot have an exact knowledge of the overall system. 
- Consider your applications and answer to the previous question: is-it really serious to lost messages time to time? What will be the consequences? Does the system will not resume by itself with the next message? (e.g. the *reply* to your *request*, or an *attributeChange* notification)
- Design state-less interfaces for your devices (e.g. no *trigger* button, but *on/off* button)
- If you are still anxious, place your critical components on the same physical device.
- And no, there will never be TCP (nor DCCP) tunnels for xAAL, ever!
