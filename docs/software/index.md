# Software

_Build It Yourself_

---
## [GitLab](https://gitlab.imt-atlantique.fr/xaal)

The Git repositories contains for now:

- xAAL specifications and schemas
- xAAL stacks in Python and in C
- gatways to various home-automation protocols (X10, KNX, Z-Wave)
- xAAL services components

Public access is read-only.
Motivated contributors are invited to contact us (see below) for a read-write access.


## Quickstart
To try xAAL and see how this works, let's run basic virtual devices on your PC!


### Implementation in [Python](https://gitlab.imt-atlantique.fr/xaal/code/python)
Go to the dedicated folder

    $ cd xaal/code/Python/

Follow [instructions](https://gitlab.imt-atlantique.fr/xaal/code/python).<br/>
Look at the list of available [packages](https://gitlab.imt-atlantique.fr/xaal/code/python/-/blob/main/packages.rst).


### Implementation in [C](https://gitlab.imt-atlantique.fr/xaal/code/c)
Go to the dedicated folder and compile

    $ cd xaal/code/C/libxaal
    more README.TXT
    make

Sart a dummy lamp:

    $ LD_LIBRARY_PATH+=:. ./dummyLamp -a 224.0.29.200 -p 1234
    Device: 1af9225a-f771-4b4a-aa77-139fe486d24c
      .  Off

In another terminal, start a dummy HMI dedicated to command lamps, play with it, and watch the effects on the dummy lamp:

    $ LD_LIBRARY_PATH+=:. ./lampCommander -a 224.0.29.200 -p 1234
    Device: 542b7e35-4545-464d-b440-1ef290f0631b

    Menu: (1) Select lamps  (2) Send on  (3) Send off  (4) Quit
    Your choice?  1
    Detected lamps:
     0:   f2bb49c3-f276-47b3-8c43-6ad737a59321 lamp.basic Fri Apr 15 14:46:39 2016
    Toggle which one? 0

    Menu: (1) Select lamps  (2) Send on  (3) Send off  (4) Quit
    Your choice?  2



### Implementation in [Go](https://gitlab.imt-atlantique.fr/xaal/code/go)
Each package has its own repository.

- xaal/code/Go/[apps](https://gitlab.imt-atlantique.fr/xaal/code/go/apps)
- xaal/code/Go/[schemas](https://gitlab.imt-atlantique.fr/xaal/code/go/schemas)
- xaal/code/Go/[uuid](https://gitlab.imt-atlantique.fr/xaal/code/go/uuid)
- xaal/code/Go/[xaal](https://gitlab.imt-atlantique.fr/xaal/code/go/xaal)



## Mailing list
Discussions about development of xAAL (lang: en, fr):
[info](https://listes.imt-atlantique.fr/wws/info/xaal) -
[subscribe](https://listes.imt-atlantique.fr/wws/subscribe/xaal).

## Legarcy code
- Software archaeologists can look at the former [svn](https://redmine.imt-atlantique.fr/svn/xaal).

## Former release
- All in a single package
[xAAL_release-0.3.tar.gz](xAAL_release-0.3.tar.gz)

## License
- The library is provided according to the termes of the [GNU Lesser General Public License v3.0 and following](https://www.gnu.org/licenses/lgpl.html)
- The demo applications are provided according to the terms of the [GNU General Public License v3.0 and following](http://www.gnu.org/copyleft/gpl.html)
- Dual-licenses are accepted, contact us.
