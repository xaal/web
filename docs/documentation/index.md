# Documentation

_xAAL Specifications_

---

## Specification
### Latest Stable
- [xAAL v0.7r2 (en)](https://gitlab.imt-atlantique.fr/xaal/specifications/-/blob/49ffc487e35f80d7f8cee997b83db9563a9f0563/xAAL-specs.pdf)

### Former
- [xAAL v0.7r1b (en)](https://gitlab.imt-atlantique.fr/xaal/specifications/-/blob/tags/specifications-0.7_r1b/xAAL-specs.pdf)
- [xAAL v0.7r1 (en)](https://gitlab.imt-atlantique.fr/xaal/specifications/-/blob/tags/specifications-0.7_r1/xAAL-specs.pdf): using cbor for messages
- [xAAL v0.6 (en)](https://gitlab.imt-atlantique.fr/xaal/specifications/-/blob/tags/specifications-0.6/xAAL-specs.pdf): using cbor for messages
- [xAAL v0.5r2 (en)](https://gitlab.imt-atlantique.fr/xaal/specifications/-/blob/tags/specifications-0.5_r2/xAAL-specs.pdf): Legacy stable
- [xAAL v0.5r1 (en)](https://gitlab.imt-atlantique.fr/xaal/specifications/-/blob/tags/specifications-0.5_r1/xAAL-specs.pdf): adding security to xAAL v0.4
- [xAAL v0.4 (en)](https://gitlab.imt-atlantique.fr/xaal/specifications/-/blob/tags/specifications-0.4_r3/xAAL-specs.pdf)
- [xAAL v0.3 (en)](https://gitlab.imt-atlantique.fr/xaal/specifications/-/blob/tags/specifications-0.3/xAAL-specs.pdf)
- [xAAL v0.2 (en)](https://gitlab.imt-atlantique.fr/xaal/specifications/-/blob/tags/specifications-0.2_en/xAAL-specs_en.pdf)
- [xAAL v0.2 (fr)](https://gitlab.imt-atlantique.fr/xaal/specifications/-/blob/tags/specifications-0.4_r2/xAAL-specs.pdf)
- [xAAL v0.1-json (fr)](https://gitlab.imt-atlantique.fr/xaal/specifications/-/blob/tags/specifications-0.1-json/protocole-xAAL.pdf)
- [xAAL v0.1-bin (fr)](https://gitlab.imt-atlantique.fr/xaal/specifications/-/blob/tags/specifications-0.1-bin/protocole_lowlevel.pdf)


## Available components
Several xAAL components are already [coded and available](../software/index.md).

Current home-automation protocols supported by our elementary gateways: X10, Z-Wave, Enocean, KNX, RFXcom, Aqara, Tuya, etc.
(Run those you need.)

Several HMI (web) are also proposed.



## Protocol
Following format specifications are provided to check the form of xAAL messages (i.e., for debugging purpose).

- xAAL v0.7: [xAAL_07_security-layer.cddl](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/security-layer.cddl) and [xAAL_07_application-layer.cddl](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/application-layer.cddl) (CDDL)
- xAAL v0.6: [xAAL_06_security-layer.cddl](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/tags/schemas-0.6/xAAL_06_security-layer.cddl) and [xAAL_06_application-layer.cddl](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/tags/schemas-0.6/xAAL_06_application-layer.cddl) (CDDL)
- xAAL v0.5: [xAAL_05_security-layer](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/tags/schemas-0.5/xAAL_05_security-layer) and [xAAL_05_application-layer](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/tags/schemas-0.5/xAAL_05_application-layer) (Json Schemas)
- xAAL v0.4: [xAAL_04_message](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/tags/schemas-0.4/xAAL_message) (Json Schema)



## Schemas
xAAL devices are described by _schemas_. These are Json object of a specific form that must validates a given template:

- xAAL v0.7: [xAAL_07_schema.cddl](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/schema.cddl) This is a [CDDL](https://tools.ietf.org/html/rfc8610) specification.
- xAAL v0.4 to v0.6: [xAAL_05_schema](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/tags/schemas-0.5/xAAL_schema) This is a [Json Schema](http://json-schema.org/).

Each xAAL devices vendor has the responsability to provide consistent schemas.


### Latest Stable Schemas (xAAL 0.7)
Details on the dedicated [Git repository](https://gitlab.imt-atlantique.fr/xaal/schemas/)

- [basic.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/basic.basic): Generic schema for any devices
- [hmi.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/hmi.basic): Basic Human Machine Interface
- [metadatadb.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/metadatadb.basic): Simple metatdata database to manage tags associated with devices
- [cache.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/cache.basic): Simple cache that can be queried about attributes of devices
- [gateway.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/gateway.basic): Simple gateway that manage physical devices
- [scenario.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/scenario.basic): Simple Scenario
- [lamp.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/lamp.basic): Simple lamp
- [lamp.dimmer](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/lamp.dimmer): Simple dimmable lamp
- [lamp.color](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/lamp.color): Color-changing lamp
- [lamp.toggle](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/lamp.toggle): Simple lamp with toggle function - Note that a toggle function may leads to undefined state due to its stateful nature; its usage should be avoided.
- [powerrelay.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/powerrelay.basic): Simple power relay device
- [powerrelay.toggle](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/powerrelay.toggle): Power relay with toggle function - Note that a toggle function may leads to undefined state due to its stateful nature; its usage should be avoided.
- [switch.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/switch.basic): Simple switch button device
- [button.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/button.basic): Simple button device
- [button.remote](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/button.remote): Simple remote device with several buttons
- [powermeter.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/powermeter.basic): Simple powermeter
- [linkquality.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/linkquality.basic): Report on quality of a transmission link
- [battery.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/battery.basic): Report on state of a battery
- [door.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/door.basic): Simple door device
- [window.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/window.basic): Simple window device
- [shutter.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/shutter.basic): Simple shutter
- [shutter.position](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/shutter.position): Shutter with a position managment
- [worktop.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/worktop.basic): Simple worktop
- [thermometer.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/thermometer.basic): Simple thermometer
- [hygrometer.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/hygrometer.basic): Simple hygrometer
- [barometer.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/barometer.basic): Simple barometer
- [co2meter.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/co2meter.basic): Simple CO2 meter
- [luxmeter.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/luxmeter.basic): Simple luxmeter
- [lightgauge.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/lightgauge.basic): Simple light gauge
- [soundmeter.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/soundmeter.basic): Simple soundmeter
- [raingauge.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/raingauge.basic): Simple rain gauge
- [windgauge.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/windgauge.basic): Simple wind gauge
- [scale.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/scale.basic): Simple scale
- [motion.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/motion.basic): Simple motion detector device
- [contact.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/contact.basic): Simple contact-sensor device; e.g. door or window opening sensor
- [falldetector.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/falldetector.basic): Simple fall detection device
- [tts.basic](https://gitlab.imt-atlantique.fr/xaal/schemas/-/blob/main/tts.basic): Text-To-Speech device
