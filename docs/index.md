# xAAL
_Home-Automation Interoperability_

---

## Introduction
Many vendors offer home-automation solutions.  There are almost as many home automation protocols as manufacturers (or alliances of manufacturers).  The consequence is a strong issue about interoperability: how to make a device from vendor A (eg.  a switch) talking with a device from vendor B (eg. a lamp)?

xAAL aims to address interoperability issues of home automation systems.

xAAL is defined by the [RAMBO][] team of [IMT-Atlantique][].

  [RAMBO]: https://www.imt-atlantique.fr/en/research-innovation/teams/research-team-rambo
  [IMT-Atlantique]: https://www.imt-atlantique.fr/

![xAAL Logo](img/logo.png)


## Mainlines
Existing solutions for interoperability issues play around the idea of gateways: a box (a small computer) is equipped with two or more modules, each module talking a given home-automation protocol ; then a piece of software over those modules make it possible to forward messages between protocol A and protocol B.

xAAL proposes to reorganize and formalize architectures of those boxes, which are in fact all composed of the same functionalities.  Functionalities are cut into well defined functional entities, communicating to each other via a messages bus over IP.  Each functional entity may have multiple instances and be deployed in different ways over physical entities.

Note that the name "xAAL" itself is not an acronym; it is the full name of the project.

![xAAL](img/xAAL_big-picture.png "xAAL")


## Principle
![xAAL Bus](img/bus-xaal.svg "xAAL Bus")

- Gateways: translate messages between a manufacturer protocol and the xAAL protocol
- Native Equipment: devices communicating using the xAAL protocol natively
- Database of Metadatas: store user key-values associated to devices; actual configuration of devices
- Cache: stores notifications of sensors about state changes
- Automaton of Scenarios: advanced home automation services
- User Interfaces: generate web pages or provides REST API for voice interface, mobile applications, or external servers
