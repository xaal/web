# Research

_Research Activities_

---

## The PRECIOUS Project
xAAL is used within [The PRECIOUS Project](http://www.thepreciousproject.eu/) to collect data from environmental sensors.

<img src="precious.png" height="50" >


## Bourses aux Technologies
xAAL was presented to the worshop "Les Bourses aux Technologies de l'Institut Mines-Télécom" - March 6, 2015, Evry, France.
See [TICsanté](http://www.ticsante.com/Numerique-en-sante-l-institut-Mines-Telecom-presente-14-innovations-technologiques-NS_2286.html)
and [Blog Recherche IMT](http://blogrecherche.wp.mines-telecom.fr/2015/02/05/domotique-et-sante-de-nouveaux-services-grace-a-linteroperabilite-des-objets-connectes/).


## CPER VITAAL <img src="feder.png" height="60" >
The xAAL project received support from the [ERDF](http://ec.europa.eu/regional_policy/en/funding/erdf/)
via the [VITAAL](http://departements.imt-atlantique.fr/info/recherche/ihsev/vitaal/) [CPER](http://www.cget.gouv.fr/contrats-de-plan-etat-region)


## Journal of Intelligent Systems, Volume 24, Issue 3 (Aug 2015)
C. Lohr, P. Tanguy, J. Kerdreux. "xAAL: A Distributed Infrastructure for Heterogeneous Ambient Devices", Journal of Intelligent Systems. Volume 24, Issue 3, Pages 321–331, ISSN (Online) 2191-026X, ISSN (Print) 0334-1860, DOI: 10.1515/jisys-2014-0144, March 2015. [http://www.degruyter.com/view/j/jisys.2015.24.issue-3/jisys-2014-0144/jisys-2014-0144.xml](http://www.degruyter.com/view/j/jisys.2015.24.issue-3/jisys-2014-0144/jisys-2014-0144.xml)


## International Conference on IoT and Big Data Technologies for HealthCare (IotCare 2016)
P. Tanguy, C. Lohr, Kerdreux. "A Transparent home Sensors/Actuators layer for Health & Well-being services", Proceedings LotCare 2016, Jun 2016, Budapest, Hungary, pp.1 - 6. [https://hal.archives-ouvertes.fr//hal-01354817](https://hal.archives-ouvertes.fr//hal-01354817) [http://eudl.eu/doi/10.1007/978-3-319-49655-9_5](http://eudl.eu/doi/10.1007/978-3-319-49655-9_5)


## 13th IEEE International Conference on Advanced and Trusted Computing (ATC 2016)
C. Lohr, P. Tanguy, J. Kerdreux. "Choosing security elements for the xAAL home automation system", IEEE Proceedings of ATC 2016, pp.534 - 541, Jul 2016, Toulouse, France. [https://hal.archives-ouvertes.fr//hal-01354837](https://hal.archives-ouvertes.fr//hal-01354837)


## KI - Künstliche Intelligenz 31(3)
S. M. Nguyen, C. Lohr, P. Tanguy, and Y. Chen. "Plug and play your robot into your smart home: Illustration of a new framework", KI - Künstliche Intelligenz, pp. 1-7, 2017. [http://dx.doi.org/10.1007/s13218-017-0494-8](http://dx.doi.org/10.1007/s13218-017-0494-8)


## EAI Endorsed Transactions on Pervasive Health and Technology 17(11): e5
P. Tanguy, M. Simonnet, C. Lohr, Kerdreux. "A Transparent home Sensors/Actuators layer for Health &Well-being services - Extended version", EAI Endorsed Transactions on Pervasive Health and Technology 17(11), Jul. 2017. [http://eudl.eu/doi/10.4108/eai.18-7-2017.152900](http://eudl.eu/doi/10.4108/eai.18-7-2017.152900)


## IEEE International Conference on Robotic Computing, 2018
P. Papadakis, C. Lohr, M. Lujak, A.-B. Karami, I. Kanellos, G. Lozenguez,A. Fleury. "System Design for Coordinated Multi-Robot Assistance Deployment in Smart Spaces", IEEE International Conference on Robotic Computing, 2018. [https://hal.inria.fr/hal-01699838/document](https://hal.inria.fr/hal-01699838/document)

## MDPI Journal Future Internet
Lohr, Christophe; Kerdreux, Jérôme. 2020. "Improvements of the xAAL Home Automation System." Future Internet 12, no. 6:104 [https://doi.org/10.3390/fi12060104](https://doi.org/10.3390/fi12060104)
