
all: site/index.html

FIGDIR = docs/img
FIGS = ${FIGDIR}/bus-xaal.fig
SVGS = $(addprefix docs/img/, $(notdir $(FIGS:%.fig=%.svg)))


site/index.html: mkdocs.yml docs/index.md docs/documentation/index.md docs/software/index.md docs/research/index.md docs/css/extra.css $(SVGS)
	# Install:  apt-get install mkdocs mkdocs-material
	mkdocs build --clean
	cp -av docs/img/favicon.ico site/img/favicon.ico

docs/img/%.svg: ${FIGDIR}/%.fig
	fig2dev -L svg -Z 12cm $< $@

docs/img/xAAL.png:
	convert -background none -size 64x64 -pointsize 32 -gravity Center -weight 700 caption:"xA AL" $@

docs/img/favicon.ico: docs/img/xAAL.png
	convert $< -background transparent -define icon:auto-resize=64,48,32,16 $@

install: 
	@echo "Copie du contenu du répertoire 'site' sur le serveur web"
	scp -r site/* xaal-web:/web/xaal/

test:
	mkdocs serve
	
proper:
	-rm -f $(SVGS) docs/*~ docs/*/*~ *~
	-rm -Rf site
	

.PHONY: all install proper test

.SUFFIXES: .md .png .fig
